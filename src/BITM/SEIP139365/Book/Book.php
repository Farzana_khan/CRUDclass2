<?php
namespace App\BITM\SEIP139365\Book;
use App\BITM\SEIP139365\Book\Utility;
use App\BITM\SEIP139365\Book\Message;
class Book

{
    public $id="";
    public $title="";
    public $conn;
    public $deleted_at; //deleted_at




    public function __construct($data="")
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectcrud2") or die("Database connection failed");
    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectcrud2`.`book` (`title`) VALUES ('".$this->title."')";
        $result=mysqli_query($this->conn,$query);
        if ($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been stored successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }
public function index()
{
    $_allbook=array();
    $query="SELECT * FROM `book`";
    $result=mysqli_query($this->conn,$query);
    while($row=mysqli_fetch_assoc($result))
    {
        $_allbook[]=$row;
    }
    return $_allbook;
}

    public function view()
    {
        $query="SELECT * FROM `book` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }




    public function create()
    {
        echo "creating";
    }

    public function delete()
    {

        $query="DELETE FROM `atomicprojectcrud2`.`book` WHERE `book`.`id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Book Title has been deleted.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "<br><br><br>Error!";
        }


    }

    public function trash()
    {
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectcrud2`.`book` SET `deleted_at` = '".$this->deleted_at."' WHERE `book`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Trashed!</strong> Book Title has been trashed.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "<br><br><br>Error!";
        }


    }
    public function trashed(){
        $_trashedBook= array();
        $query="SELECT * FROM `book` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);

        while($row= mysqli_fetch_assoc($result)){
            $_trashedBook[]=$row;
        }

        return $_trashedBook;

    }

    public function recover(){
        $query="UPDATE `atomicprojectcrud2`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Data has been recovered successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function recoverMultiple($idS=array()){
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="UPDATE `atomicprojectcrud2`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("<div class=\"alert alert-success\">
  <strong>Recovered!</strong> Selected Data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
    </div>");
                Utility::redirect('index.php');

            }



        }

    }
    public function deleteMultiple($idS=array())
    {
        if((is_array($idS)) && count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectcrud2`.`book`  WHERE `book`.`id` IN(".$IDs.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("
                <div class=\"alert alert-success\">
                <strong>Deleted!</strong> Selected Data has been deleted successfully.
                </div>");
                Utility::redirect('index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong>
            </div>");


            }


        }



    }


public function update()
    {
        $query = "UPDATE `atomicprojectcrud2`.`book` SET `title` = '".$this->title."' WHERE `book`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Updated!</strong> Book Title has been updated.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "<br><br><br>Error!";
        }
    }


    public function prepare($data=""){
        if(array_key_exists("title",$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        //echo  $this;

    }

}