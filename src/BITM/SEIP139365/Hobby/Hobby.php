<?php
namespace App\BITM\SEIP139365\Hobby;
use App\BITM\SEIP139365\Hobby\Message;
use App\BITM\SEIP139365\Hobby\Utility;

class Hobby
{
    public $id="";
    public $hobby="";
    public $conn;

    public function __construct($data="")
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectcrud2") or die("Database connection failed");
    }

    public function prepare($data=""){
        if(array_key_exists("hobby",$data)){
            $this->title=$data['hobby'];
        }
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        //echo  $this;

    }
    public function store()
    {
        $query="INSERT INTO `atomicprojectcrud2`.`hobby` (`title`) VALUES ('".$this->title."')";
        $result=mysqli_query($this->conn,$query);
        if ($result)
        {
            Message::message("
        <div class=\"alert alert-success\">
        <strong>Success!</strong> Data has been stored successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "Error!!!";
        }
    }

    public function index()
    {
        $_allhobby=array();
        $query="SELECT * FROM `hobby`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_allhobby[]=$row;
        }
        return $_allhobby;
    }

    public function view()
    {
        $query="SELECT * FROM `hobby` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }


    public function update()
    {
        $query = "UPDATE `atomicprojectcrud2`.`hobby` SET `title` = '".$this->title."' WHERE `hobby`.`id` =".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Hobby Updated!</strong> Hobby has been updated.
            </div>");
            Utility::redirect('index.php');
           // header('Location:index.php');
        }
        else
        {
            echo "<br><br><br>Error!";
        }
    }

    public function delete()
    {

        $query="DELETE FROM `atomicprojectcrud2`.`hobby` WHERE `hobby`.`id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Book Title has been deleted.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "<br><br><br>Error!";
        }


    }





}