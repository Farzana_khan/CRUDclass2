<?php
namespace App\BITM\SEIP139365\Email;
//use App\BITM\SEIP139365\Email\Email;

use App\BITM\SEIP139365\Email\Message;
use App\BITM\SEIP139365\Email\Utility;

class Email
{
    public $id;
    public $address;
    public $conn;
    public $deleted_at;


    public function prepare($data = "")
    {
        if (array_key_exists("id", $data))
        {
            $this->id = $data['id'];
        }


        if (array_key_exists("address", $data))
        {
            $this->address = $data['address'];
        }


    }

    public function __construct($data="")
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectcrud2") or die("Database has not been connected");
    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectcrud2`.`email` (`address`) VALUES ('".$this->address."')";
        //echo $query;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Data has been stored successfully");
           // Utility::redirect("index.php");
            header('Location:index.php');
        }
        else
        {
            echo "Error in data insertion";
        }


    }

    public function index()

    {
        $_allemail = array();
        $query = "SELECT * from `email`";
        $result = mysqli_query($this->conn, $query);
        while ($row=mysqli_fetch_assoc($result))
        {
            $_allemail[] = $row;

        }
        return $_allemail;
    }

    public function view()
    {
        $query="SELECT * FROM `email` WHERE `email`.`id`= ".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {

       // $query="UPDATE `atomicprojectcrud2`.`email` SET `address`='".$this->id."' WHERE `email`.`address`= ".$this->id;
        $query="UPDATE `atomicprojectcrud2`.`email` SET `address`='".$this->address."' WHERE `email`.`address`= ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Data has been updated successfully.");
            header('Location:index.php');
        }
        else
        {
            echo "ERROR!!";
        }

    }

    public function delete()
    {
      //  $query="DELETE FROM `atomicprojectcrud2`.`email` WHERE `email`.`address`=".$this->id;
        $query="DELETE FROM `atomicprojectcrud2`.`email` WHERE `email`.`id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Data has been deleted successfully!");
            header('Location:index.php');
        }
        else
        {
            echo "Error in deletion";

        }
    }

    public function trash()
{
    $query="UPDATE `atomicprojectcrud2`.`email` SET `deleted_at`='".$this->id."' WHERE `email`.`deleted_at`= ".$this->id;
    $result=mysqli_query($this->conn,$query);
    if($result)
    {
        Message::message("Data has been trashed");
        header('Location:index.php');

    }
    else
    {
        echo "error in trashing";
    }
}











}

