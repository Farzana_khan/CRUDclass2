<?php
namespace App\BITM\SEIP139365\Email;
//use App\BITM\SEIP139365\Email\Message;
if(!isset($_SESSION['message']))
{
    session_start();
}

class Message
{
    public static function message($message=NULL)
    {
        if (is_null($message))
        {
            $message = self::getMessage();
            return $message;
        } else
        {
            self::setMessage($message);
        }
    }


public static function getMessage()
{
   $message=$_SESSION['message'];
    $_SESSION['message']="";
    return $message;

}

public static function setMessage($message)
{
     $_SESSION['message']=$message;
}


}