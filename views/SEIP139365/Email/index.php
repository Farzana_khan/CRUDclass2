
<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Email\Email;
use App\BITM\SEIP139365\Email\Utility;
use App\BITM\SEIP139365\Email\Message;

$email=new Email();
$allemail=$email->index();



?>

<!DOCTYPE html>
<body>
<div class="container">
    <h1>Email Index Here: </h1>
    <a href="create.php" role="button" class="btn btn-primary" >Insert Email Again</a>

    <a href="trash.php" role="button" >Goto Trash </a>

</div>

<div class="table-responsive">
    <br> <br>
    <table class="table">
    <tr>
        <thead>
        <th>#</th>
        <th>Serial</th>
        <th>Email Address</th>
        <th>Action</th>
        </thead>
    </tr>

        <tbody>
        <tr>
            <?php $sl=0;
            foreach($allemail as $email)

            { $sl++;  ?>

            <td><?php echo $sl ?> </td>
            <td> <?php echo $email['id']?></td>
            <td> <?php echo $email['address']?></td>


        <td> <a href="view.php?id=<?php $book['id']?>" role="button" class="btn btn-primary">VIEW</a> </td>
        <td> <a href="edit.php?id=<?php $book['id']?>" role="button" class="btn btn-primary">EDIT</a> </td>
        <td> <a href="delete.php?id=<?php $book['id']?>" role="button" class="btn btn-primary">DELETE</a> </td>
        <td> <a href="trash.php?id=<?php $book['id']?>" role="button" class="btn btn-primary">TRASH</a> </td>
        </tr>

        <?php  } ?>

        </tbody>


    </table>
</div>
</body>

</html>