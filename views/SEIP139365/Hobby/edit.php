<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139365\Hobby\Hobby;
use App\Bitm\SEIP139365\Utility\Utility;

$hobby = new Hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->view();
$hobby_name= $singleItem['hobby'];

$hobby_array=explode(",",$hobby_name);
print_r($hobby_array);





//Utility::dd($singleBook);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit your hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>



<body>

<div class="container">

    <h2>Edit your hobby</h2>

    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit your hobby:</label>
            <input type="hidden" name="id" id="hobby" value="<?php echo $singleItem['id']?>">
            <input type="hidden" name="title" id="hobby" value="<?php echo $singleItem['id']?>">


            <div class="checkbox">
                <label><input type="checkbox" name=hobby[] value="Gardening" <?php if(in_array('Gardening',$hobby_array))echo 'checked';?> "</label>
            </div>


            <div class="checkbox">
                <label><input type="checkbox" name=hobby[] value="Playing Cricket" <?php if(in_array('Playing Cricket',$hobby_array)) echo 'checked';?>>Playing Cricket</label>
            </div>

            <div class="checkbox">
                <label><input type="checkbox" name=hobby[] value="Reading Books" <?php if(in_array('Reading Books',$hobby_array)) echo 'checked';?>>Reading Books</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name=hobby[] value="Travelling" <?php if(in_array('Travelling',$hobby_array)) echo 'checked';?>>Travelling</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name=hobby[] value="Swimming" <?php if(in_array('Swimming',$hobby_array)) echo 'checked';?>>Swimming</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name=hobby[] value="Browsing" <?php if(in_array('Browsing',$hobby_array)) echo 'checked';?>>Browsing</label>
            </div>








            <button type="submit" class="btn btn-default">Update</button>

    </form>
</div>

</body>
</html>