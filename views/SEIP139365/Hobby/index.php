<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP139365\Hobby\Hobby;
use App\Bitm\SEIP139365\Hobby\Message;
use App\Bitm\SEIP139365\Hobby\Utility;

$hobby= new Hobby();
$allhobby=$hobby->index();
//Utility::d($allBook);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All Hobby List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Insert again</a>
    <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a><br><br>

    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>


    <div class="table-responsive">

        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Book title</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allhobby as $hobby){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $hobby['id']?></td>
                <td><?php echo $hobby['title']?></td>
                <td><a href="view.php?id=<?php echo $hobby['id']?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $hobby['id']?>" class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $hobby['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $hobby['id']?>" class="btn btn-info  btn-xs" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();
</script>


</body>
</html>
